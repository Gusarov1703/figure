package ru.demaFigure;

public enum Color {
    red,
    blue,
    white,
    black,
    green,
    yellow
}
